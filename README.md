# Origami 2 stack socket.io helper

## constructor

Params:

- `privateKey` **requred**: stack private key

## .getStack

Returns: underlying *stack* instance

## .getNameRegistry

Returns: underlying *name registry* instance

## .authorizePlugin

Params:

- `plugin name` **required**: plugin name
- `public key` **required**: plugin public key

## .authorizeTokenProvider

Params:

- `plugin name` **required**: token provider name
- `public key` **required**: token provider public key

## .listenServer

Creates a [socket.io](http://socket.io/) that listens to the node.js http or https server and attaches to it using `.listenIo`.

Params:

- `server` **required**: http or https node.js style server

## .listenIo

Attaches to a [socket.io instance](http://socket.io/) object. For each connected socket, invokes `.listenSocket`.

Params:

- `io` **required**: [socket.io instance](http://socket.io/) object instance

## .listenSocket

Handles incoming connections on the socket instance. It is a node.js event-emitter or a socket.io style socket object.

Params:

- `socket` **required**: socket to attach the stack to
